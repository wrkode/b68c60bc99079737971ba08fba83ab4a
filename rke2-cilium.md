# Install the Official Cilium Chart on RKE2
1. Create a file `/etc/rancher/rke2/config.yaml`
    ```yaml
    cni: none
    disable:
    - rke2-ingress-nginx
    ```
2. Apply the following manifest
    ```yaml
    apiVersion: helm.cattle.io/v1
    kind: HelmChart
    metadata:
      name: cilium
      namespace: kube-system
    spec:
      chart: https://github.com/cilium/charts/raw/master/cilium-1.14.1.tgz
      bootstrap: true
      valuesContent: |-
        kubeProxyReplacement: strict
        k8sServiceHost: 127.0.0.1
        k8sServicePort: 6443
        encryption:
          enabled: true
          nodeEncryption: true
          type: wireguard
        ingressController:
          enabled: true
        envoy:
          enabled: true
    ```
# L2 Announcements
1. Add the following components to enable the feature
    ```yaml
    apiVersion: helm.cattle.io/v1
    kind: HelmChart
    metadata:
      name: cilium
      namespace: kube-system
    spec:
      chart: https://github.com/cilium/charts/raw/master/cilium-1.14.1.tgz
      valuesContent: |-
        [...]
        l2announcements:
          enabled: true
        externalIPs:
          enabled: true
        devices: 'eth0'
    ```
1. Create the following manifests to configure it
    ```yaml
    apiVersion: "cilium.io/v2alpha1"
    kind: CiliumL2AnnouncementPolicy
    metadata:
      name: policy
      namespace: kube-system
    spec:
      nodeSelector:
        matchExpressions:
        - key: node-role.kubernetes.io/control-plane
          operator: DoesNotExist
      interfaces:
      - eth0
      loadBalancerIPs: true
      externalIPs: true
    ---
    apiVersion: "cilium.io/v2alpha1"
    kind: CiliumLoadBalancerIPPool
    metadata:
      name: "home-pool"
    spec:
      cidrs:
      - cidr: "192.168.178.0/24"
    ```
# Sources
- https://medium.com/@rudradevpal/cilium-1-14-unleashing-the-power-of-l2-announcements-for-kubernetes-networking-981ee98e080d
- https://docs.cilium.io/en/stable/network/servicemesh/ingress/
- https://isovalent.com/blog/post/cilium-release-114/#h-envoy-daemonset
- https://docs.cilium.io/en/stable/network/l2-announcements
